# Récupération des composants

Cette application utilise Composer, un gestionnaire de composants pour PHP.
Pour récupérer les composants requis par Lille1TeacherBook, exécutez les instructions suivantes :

- Installez [Composer](http://getcomposer.org/) si ce n'est pas encore fait.
- Une fois à la racine du dossier, exécutez 'php composer.phar install'

# Configuration

Le fichier **config.php.example** contient un exemple de fichier de configuration. Copiez-le en **config.php** et modifiez les informations qu'il contient pour correspondre aux informations de connexion à la base de données.

# Installation de la base de données

Dans la base de données précisée par **config.php**, exécutez les commandes présentes dans dump.sql.
Vous obtiendrez une table "teacher" contenant des enseignants exemples.