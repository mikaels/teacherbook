<?php
require 'vendor/autoload.php';
require 'config.php';

global $app;
$app = new \Slim\Slim();

/* ============================== */
/* Routing                        */
/* ============================== */
$app->get('/teacher/:login', 'getTeacher');

$app->run();

/* ============================== */
/* Services                       */
/* ============================== */

function getTeacher($login) {

    $teacher = getTeacherFromLogin($login);

    if (is_null($teacher)) {
        return throw404Error();
    } else {
        return echoTeacher($teacher);
    }
}

/* ============================== */
/* Responses                      */
/* ============================== */

function echoTeacher($teacher) {
    echo json_encode($teacher);
}

function throw404Error() {
    global $app;
    $error = array(
        'error' => '404 : Teacher not found'
    );
    $app->response()->status(404);
    echo json_encode($error);

    return false;
}



/* ============================== */
/* Link to DB / SQL               */
/* ============================== */
function getTeacherFromLogin($login) {
    $pdo = getPDOObject();
    $query = $pdo -> prepare(getTeacherFromLoginQuery());
    $query->bindParam(":login", $login);
    $query->execute();
    $teacher = $query -> fetch(PDO::FETCH_ASSOC);

    if ($teacher) {
        return $teacher;
    } else {
        return null;
    }
}

function getTeacherFromLoginQuery() {
    return "SELECT login,email,firstName,lastName FROM teacher WHERE login = :login";
}

/** @return PDO */
function getPDOObject() {
    try {
        $pdo = new PDO('mysql:host='.DB_HOST.';port='.DB_PORT.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PASSWORD);
        return $pdo;
    } catch (Exception $e) {
        die('Unable to connect to DB. Reason : '.$e);
    }
}
