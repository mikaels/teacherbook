SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

create database IF NOT EXISTS lille1teacherbook;
use lille1teacherbook;

CREATE TABLE IF NOT EXISTS `teacher` (
  `login` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) DEFAULT NULL,
  `firstName` varchar(200) DEFAULT NULL,
  `lastName` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `teacher`
--

INSERT INTO `teacher` (`login`, `email`, `firstName`, `lastName`) VALUES
('hopper', 'Grace.Hopper@navy.gov', 'Grace', 'Hopper'),
('turing', 'Alan.Turing@enigma.uk', 'Alan', 'Turing');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `teacher`
--
ALTER TABLE `teacher`
 ADD PRIMARY KEY (`login`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
